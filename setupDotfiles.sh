#!/bin/bash
############################
# setupDotfiles.sh
# Creates symlinks from the home directory to any desired dotfiles in ~/dotfiles
############################

########## Variables

dir=~/dotfiles                    # dotfiles directory
olddir=~/dotfiles_old             # old dotfiles backup directory
files="vimrc bash_profile bashrc "    # list of files/folders to symlink in homedir

##########

# create dotfiles_old in homedir
echo "Creating $olddir for backup of any existing dotfiles in ~"
mkdir -p $olddir
mkdir -p $olddir/awesome
echo "...done"

# change to the dotfiles directory
echo "Changing to the $dir directory"
cd $dir
echo "...done"

# move any existing dotfiles in homedir to dotfiles_old directory, then create symlinks 
for file in $files; do
    echo "Moving any existing dotfiles from ~ to $olddir"
    mv ~/.$file ~/dotfiles_old/
    echo "Creating symlink to $file in home directory."
    ln -s $dir/$file ~/.$file
done

# change to the dotfiles directory
echo "Changing to the $dir/awesome directory"
cd awesome
echo "...done"

# move any existing awesomewm files in homedir to dotfiles_old/awesome, then create symlink
echo "Moving any existing dotfiles from ~/.config/awesome to $olddir/awesome"
mv ~/.config/awesome/* ~/dotfiles_old/awesome
rm -Rf ~/.config/awesome
echo "Creating symlink to awesome in .config directory."
ln -s $dir/awesome ~/.config


